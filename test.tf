provider "aws" {
  region = "ap-south-1" # Specify your desired AWS region
}

resource "aws_instance" "example" {
  ami           = "ami-0287a05f0ef0e9d9a" # Specify the AMI ID for your instance
  instance_type = "t2.micro"              # Specify the instance type

  tags = {
    Name = "example-instance"
  }
}
